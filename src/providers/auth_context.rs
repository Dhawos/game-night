use leptos::{
    create_local_resource, create_server_action, Action, Resource, ServerFnError, SignalGet,
};
use crate::api::user::{get_user_info, Login, Logout, UserResponse};

#[derive(Clone)]
pub struct AuthContext {
    pub user: Resource<(usize, usize), Result<Option<UserResponse>, ServerFnError>>,
    pub login: Action<Login, Result<(), ServerFnError>>,
    pub logout: Action<Logout, Result<(), ServerFnError>>,
}

impl AuthContext {
    pub fn new() -> Self {
        let login = create_server_action::<Login>();
        let logout = create_server_action::<Logout>();
        let user = create_local_resource(
            move || (login.version().get(), logout.version().get()),
            move |_| get_user_info(),
        );
        Self {
            user,
            login,
            logout,
        }
    }
}

