use std::fs;

pub fn load_raw(path: &str) -> String {
    fs::read_to_string(path).unwrap_or_else(|_| panic!("Error reading file {}", path))
}

#[cfg(feature = "ssr")]
fn main() {
    use diesel::prelude::*;
    use game_night::db::bgg_game::BGGGame;
    use std::env;

    use dotenvy::dotenv;
    let path = std::env::args().nth(1).expect("No file given");
    let data = load_raw(&path);
    let mut rdr = csv::Reader::from_reader(data.as_bytes());
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let mut connection = PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));
    let games: Vec<BGGGame> = rdr
        .deserialize()
        .map(|x| {
            let y: BGGGame = x.unwrap();
            y
        })
        .collect();

    for chunk in games.chunks(10000) {
        let result = diesel::insert_into(game_night::db::schema::bgg_games::table)
            .values(chunk)
            .execute(&mut connection);
        result.unwrap();
    }
}
