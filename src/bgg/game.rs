use leptos::ServerFnError;
use reqwest::get;
use serde::{Deserialize, Serialize};
use serde_xml_rs::from_str;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct BoardGames {
    #[serde(rename = "$value")]
    boardgames: Vec<BoardGame>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct BoardGame {
    pub description: String,
    pub thumbnail: String,
    pub image: String,
    pub minplayers: u16,
    pub maxplayers: u16,
    pub playingtime: u16,
}

pub async fn get_game_info(game_id: i32) -> Result<BoardGame, ServerFnError> {
    let url = format!("https://boardgamegeek.com/xmlapi/boardgame/{}", game_id);
    let body = get(url).await?;
    let text = &body.text().await?;
    let board_games: BoardGames = from_str(text)?;
    Ok(board_games.boardgames[0].clone())
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::test;

    #[test]
    async fn test_bgg_api() {
        let tm_id = 167791;
        let board_game = get_game_info(tm_id).await.unwrap();
        let expected = BoardGame{
            thumbnail: "https://cf.geekdo-images.com/wg9oOLcsKvDesSUdZQ4rxw__thumb/img/BTxqxgYay5tHJfVoJ2NF5g43_gA=/fit-in/200x150/filters:strip_icc()/pic3536616.jpg".to_string(),
            image: "https://cf.geekdo-images.com/wg9oOLcsKvDesSUdZQ4rxw__original/img/thIqWDnH9utKuoKVEUqveDixprI=/0x0/filters:format(jpeg)/pic3536616.jpg".to_string(),
            minplayers: 1,
            maxplayers: 5,
            description: "In the 2400s, mankind begins to terraform the planet Mars. Giant corporations, sponsored by the World Government on Earth, initiate huge projects to raise the temperature, the oxygen level, and the ocean coverage until the environment is habitable. In Terraforming Mars, you play one of those corporations and work together in the terraforming process, but compete for getting victory points that are awarded not only for your contribution to the terraforming, but also for advancing human infrastructure throughout the solar system, and doing other commendable things.<br/><br/>The players acquire unique project cards (from over two hundred different ones) by buying them to their hand. The cards can give you immediate bonuses, as well as increasing your production of different resources. Many cards also have requirements and they become playable when the temperature, oxygen, or ocean coverage increases enough. Buying cards is costly, so there is a balance between buying cards and actually playing them. Standard Projects are always available to complement your cards. Your basic income, as well as your basic score, is based on your Terraform Rating. However, your income is complemented with your production, and you also get VPs from many other sources.<br/><br/>Each player keeps track of their production and resources on their player boards, and the game uses six types of resources: MegaCredits, Steel, Titanium, Plants, Energy, and Heat. On the game board, you compete for the best places for your city tiles, ocean tiles, and greenery tiles. You also compete for different Milestones and Awards worth many VPs. Each round is called a generation and consists of the following phases:<br/><br/>1) Player order shifts clockwise.<br/>2) Research phase: All players buy cards from four privately drawn.<br/>3) Action phase: Players take turns doing 1-2 actions from these options: Playing a card, claiming a Milestone, funding an Award, using a Standard project, converting plant into greenery tiles (and raising oxygen), converting heat into a temperature raise, and using the action of a card in play. The turn continues around the table (sometimes several laps) until all players have passed.<br/>4) Production phase: Players get resources according to their terraform rating and production parameters.<br/><br/>When the three global parameters (temperature, oxygen, ocean) have all reached their goal, the terraforming is complete, and the game ends after that generation. Count your Terraform Rating and other VPs to determine the winning corporation!<br/><br/>".to_string(),
            playingtime: 120,
        };
        assert_eq!(board_game, expected);
    }
}
