use leptos::{server, ServerFnError};
use serde::{Deserialize, Serialize};

#[cfg(feature = "ssr")]
use crate::db::game::Game;
#[cfg(feature = "ssr")]
use crate::db::game::NewGame;
#[cfg(feature = "ssr")]
use crate::db::bgg_game::BGGGame;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GameResponse {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub thumbnail: Option<String>,
    pub image: Option<String>,
    pub min_players: Option<i32>,
    pub max_players: Option<i32>,
    pub playing_time: Option<i32>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GameSuggestionResponse {
    pub id: i32,
    pub name: String,
}


#[cfg(feature = "ssr")]
impl From<&BGGGame> for GameSuggestionResponse {
    fn from(value: &BGGGame) -> Self {
        GameSuggestionResponse {
            id: value.id,
            name: value.name.clone(),
        }
    }
}

#[cfg(feature = "ssr")]
impl From<&(Game,BGGGame)> for GameResponse {
    fn from(value: &(Game, BGGGame)) -> Self {
        GameResponse {
            id: value.0.id,
            name: value.1.name.clone(),
            description: value.1.description.clone(),
            thumbnail: value.1.thumbnail.clone(),
            image: value.1.image.clone(),
            min_players: value.1.min_players,
            max_players: value.1.max_players,
            playing_time: value.1.playing_time,
        }
    }
}

#[server(ListGames, "/games", "GetJson", "list")]
pub async fn list_games() -> Result<Vec<GameResponse>, ServerFnError> {
    use crate::api::db::pool;
    use crate::api::user::get_user_from_db;
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos_actix::ResponseOptions;
    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let user_id: i32 = user.id()?.parse::<i32>()?;
            let user_option = get_user_from_db(user_id).await?;
            match user_option {
                Some(user) => Ok(Game::query_games(&pool, &user)
                    .await?
                    .to_vec()
                    .iter()
                    .map(|response| GameResponse::from(response))
                    .collect()),
                None => Err(ServerFnError::new("User not found in db")),
            }
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(vec![])
        }
    }
}

#[server(SearchGames, "/games", "GetJson", "search")]
pub async fn search_games(query: String) -> Result<Vec<GameSuggestionResponse>, ServerFnError> {
    use crate::api::db::pool;
    let pool = pool()?;
    Ok(BGGGame::search_games(query.to_lowercase(), &pool)
        .await?
        .to_vec()
        .iter()
        .map(GameSuggestionResponse::from)
        .collect())
}

#[server(CreateGame, "/games", "Url", "new")]
pub async fn create_game(game_id: i32) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos_actix::ResponseOptions;
    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let user_id: i32 = user.id()?.parse::<i32>()?;
            let new_game = NewGame {
                owner_id: user_id,
                bgg_game_id: game_id,
            };
            new_game.create_game(&pool).await?;
            // Update the game in our BGG table
            let mut bgg_game = BGGGame::get_by_id(game_id, &pool).await?.ok_or(ServerFnError::new("could not update game"))?;
            update_bgg_game_info(&mut bgg_game).await?;
            Ok(())
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(())
        }
    }
}

#[server(DeleteGame, "/games", "Url", "delete")]
pub async fn delete_game(id: i32) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    let pool = pool()?;
    Game::delete_by_id(id, &pool).await
}

#[cfg(feature = "ssr")]
pub async fn update_bgg_game_info(game: &mut BGGGame) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use crate::bgg::game::get_game_info;
    let pool = pool()?;
    let game_info = get_game_info(game.id).await.unwrap();
    game.description = Some(game_info.description);
    game.thumbnail = Some(game_info.thumbnail);
    game.image = Some(game_info.image);
    game.min_players = Some(game_info.minplayers.into());
    game.max_players = Some(game_info.maxplayers.into());
    game.playing_time = Some(game_info.playingtime.into());
    game.update_game(&pool).await?;
    Ok(())
}
