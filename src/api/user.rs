use leptos::{server, ServerFnError};
use serde::{Deserialize, Serialize};

#[cfg(feature = "ssr")]
use crate::db::user::User;
#[cfg(feature = "ssr")]
use crate::db::user::NewUser;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct UserResponse {
    pub id: i32,
    pub username: String,
}

#[cfg(feature = "ssr")]
impl UserResponse {
    fn from_id(id: &str) -> Result<Option<Self>, ServerFnError> {
        use crate::api::db::pool;
        let pool = pool()?;
        let id_int = id.parse::<i32>()?;
        Ok(User::get_by_id(id_int, &pool)?.map(|x| UserResponse {
            username: x.username,
            id: x.id,
        }))
    }
}

#[server(GetUser, "/auth", "GetJson", "me")]
pub async fn get_user_info() -> Result<Option<UserResponse>, ServerFnError> {
    use actix_identity::IdentityExt;
    use actix_web::HttpRequest;
    use leptos::use_context;
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    let id = match identity {
        Ok(id) => id.id()?,
        Err(_) => return Ok(None),
    };
    let response = UserResponse::from_id(&id)?.ok_or(ServerFnError::new("User not found"))?;
    Ok(Some(response))
}

#[cfg(feature = "ssr")]
pub async fn get_user_from_db(user_id: i32) -> Result<Option<User>, ServerFnError> {
    use crate::api::db::pool;
    let pool = pool()?;
    let user = User::get_by_id(user_id, &pool)?;
    Ok(user)
}

#[server(Login, "/auth", "Url", "login")]
pub async fn login(username: String, password: String) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use actix_identity::Identity;
    use actix_web::http::StatusCode;
    use actix_web::HttpMessage;
    use argon2::{
        password_hash::{PasswordHash, PasswordVerifier},
        Argon2,
    };
    use leptos::expect_context;
    use leptos::use_context;
    use leptos_actix::ResponseOptions;

    let pool = pool()?;
    let user = User::get_by_username(&username, &pool)?;
    let response = expect_context::<ResponseOptions>();

    match user {
        Some(user) => {
            let parsed_hash =
                PasswordHash::new(&user.hashed_password).map_err(ServerFnError::new)?;
            let result = Argon2::default()
                .verify_password(password.as_bytes(), &parsed_hash)
                .map_err(|_| ServerFnError::new("Invalid username or password"));
            match result {
                Ok(_) => {
                    let req = use_context::<actix_web::HttpRequest>()
                        .ok_or("Failed to get HttpRequest")
                        .map_err(ServerFnError::new)?;
                    Identity::login(&req.extensions(), user.id.to_string())?;
                    leptos_actix::redirect("/");
                    Ok(())
                }
                Err(err) => {
                    response.set_status(StatusCode::UNAUTHORIZED);
                    Err(err)
                }
            }
        }
        None => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(())
        }
    }
}

#[server(Register, "/auth", "Url", "register")]
pub async fn register(
    username: String,
    email: String,
    display_name: String,
    password: String,
    password_confirmation: String,
) -> Result<(), ServerFnError> {
    use argon2::{
        password_hash::{rand_core::OsRng, PasswordHasher, SaltString},
        Argon2,
    };
    const MIN_PASSWORD_LENGTH: usize = 8;
    const MAX_PASSWORD_LENGTH: usize = 128;
    if password != password_confirmation {
        return Err(ServerFnError::new(
            "Password and password confirmation don't match",
        ));
    }

    if password.len() < MIN_PASSWORD_LENGTH {
        return Err(ServerFnError::new(format!(
            "Password too short, minimum {} characters",
            MIN_PASSWORD_LENGTH
        )));
    }

    if password.len() > MAX_PASSWORD_LENGTH {
        return Err(ServerFnError::new(format!(
            "Password too long, maximum {} characters",
            MAX_PASSWORD_LENGTH
        )));
    }

    let argon2 = Argon2::default();
    let salt = SaltString::generate(&mut OsRng);
    let hashed_password = argon2
        .hash_password(password.as_bytes(), &salt)
        .map_err(ServerFnError::new)?
        .to_string();

    let new_user = NewUser::new(&username, &email, &display_name, &hashed_password);

    use crate::api::db::pool;
    let pool = pool()?;
    new_user.create(&pool).await?;
    leptos_actix::redirect("/");
    Ok(())
}

#[server(Logout, "/auth", "Url", "logout")]
pub async fn logout() -> Result<(), ServerFnError> {
    use actix_session::Session;
    use leptos_actix::extract;

    let session: Session = extract().await?;
    session.clear();
    leptos_actix::redirect("/");
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::test;

    #[test]
    async fn test_password_too_small() {
        let result = register(
            "".to_string(),
            "".to_string(),
            "".to_string(),
            "test".to_string(),
            "".to_string(),
        )
        .await;
        assert!(result.is_err());
    }

    #[test]
    async fn test_password_too_large() {
        let result = register("".to_string(), "".to_string(), "".to_string(), "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".to_string(), "".to_string()).await;
        assert!(result.is_err());
    }
}
