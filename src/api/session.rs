use chrono::NaiveDateTime;
use leptos::{server, ServerFnError};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SessionResponse {
    pub id: i32,
    pub owner_id: i32,
    pub date_time: NaiveDateTime,
    pub duration: i32,
}

#[cfg(feature = "ssr")]
use crate::db::session::Session;

#[cfg(feature = "ssr")]
impl From<&Session> for SessionResponse {
    fn from(value: &Session) -> Self {
        SessionResponse {
            id: value.id,
            owner_id: value.owner_id,
            date_time: value.date_time,
            duration: value.duration,
        }
    }
}

#[server(CreateSession, "/sessions", "Url", "new")]
pub async fn create_session(datetime: String, duration: i32) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use crate::db::session::NewSession;
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos::ServerFnError;
    use leptos_actix::ResponseOptions;
    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    let parsed_date_time = chrono::NaiveDateTime::parse_from_str(&datetime, "%Y-%m-%dT%H:%M")?;
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let user_id: i32 = user.id()?.parse::<i32>()?;
            let new_session = NewSession {
                owner_id: user_id,
                date_time: parsed_date_time,
                duration,
            };
            if new_session.validate() {
                new_session.create_session(&pool).await?;
                return Ok(());
            }
            Err(ServerFnError::new("invalid session"))
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(())
        }
    }
}

#[server(GetSession, "/sessions", "Url", "get")]
pub async fn get_session(session_id: i32) -> Result<Option<SessionResponse>, ServerFnError> {
    use crate::api::db::pool;
    use crate::db::session::NewSession;
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos::ServerFnError;
    use leptos_actix::ResponseOptions;
    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let session_option = Session::get_by_id(session_id, &pool)?;
            match session_option {
                Some(session) => {
                    let user_id: i32 = user.id()?.parse::<i32>()?;
                    let session_response = SessionResponse::from(&session); 
                    if user_id != session_response.owner_id {
                        response.set_status(StatusCode::UNAUTHORIZED);
                        return Ok(None)
                    }
                    Ok(Some(SessionResponse::from(&session)))
                }
                None => {
                    response.set_status(StatusCode::UNAUTHORIZED);
                    Ok(None)
                }
            }
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(None)
        }
    }
}
#[server(InviteToSession, "/sessions", "Url", "invite")]
pub async fn invite_to_session(session_id: i32, invited_user_id: i32) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use crate::db::{session::NewSessionInvitation, user::User};
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos::ServerFnError;
    use leptos_actix::ResponseOptions;

    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let user_id: i32 = user.id()?.parse::<i32>()?;
            let session = Session::get_by_id(session_id, &pool)?
                .ok_or(ServerFnError::new("session does not exists"))?;
            User::get_by_id(user_id, &pool)?
                .ok_or(ServerFnError::new("invited user does not exists"))?;
            if session.owner_id != user_id {
                return Err(ServerFnError::new("user is not owner of given session_id"));
            };
            let new_invitation = NewSessionInvitation::new(invited_user_id, session_id);
            new_invitation.create_invitation(&pool)?;
            Ok(())
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(())
        }
    }
}

#[server(ListSessions, "/sessions", "GetJson", "list")]
pub async fn list_sessions() -> Result<Vec<SessionResponse>, ServerFnError> {
    use crate::api::db::pool;
    use crate::api::user::get_user_from_db;
    use actix_identity::IdentityExt;
    use actix_web::http::StatusCode;
    use actix_web::HttpRequest;
    use leptos::expect_context;
    use leptos::use_context;
    use leptos_actix::ResponseOptions;
    let response = expect_context::<ResponseOptions>();
    let req = use_context::<HttpRequest>().ok_or(ServerFnError::new("Could not get request"))?;
    let identity = IdentityExt::get_identity(&req);
    match identity {
        Ok(user) => {
            let pool = pool()?;
            let user_id: i32 = user.id()?.parse::<i32>()?;
            let user_option = get_user_from_db(user_id).await?;
            match user_option {
                Some(user) => Ok(Session::query_sessions(&pool, &user)
                    .await?
                    .to_vec()
                    .iter()
                    .map(SessionResponse::from)
                    .collect()),
                None => Err(ServerFnError::new("User not found in db")),
            }
        }
        Err(_) => {
            response.set_status(StatusCode::UNAUTHORIZED);
            Ok(vec![])
        }
    }
}

#[server(DeleteSession, "/sessions", "Url", "delete")]
pub async fn delete_session(id: i32) -> Result<(), ServerFnError> {
    use crate::api::db::pool;
    use crate::db::session::Session;
    let pool = pool()?;
    Session::delete_by_id(id, &pool)
}
