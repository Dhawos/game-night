use crate::ui::components::game::GameList;
use crate::ui::components::session::SessionsList;
use leptos::*;
use leptos_meta::*;
use leptos_router::*;

use crate::providers::auth_context::AuthContext;
use crate::ui::components::bgg_credit::BGGCredit;
use crate::ui::components::navbar::Navbar;
use crate::ui::pages::login::LoginPage;
use crate::ui::pages::library::LibraryPage;
use crate::ui::pages::sessions::{SessionsPage,SessionEditPage};
use crate::ui::pages::register::RegisterPage;

#[component]
pub fn App() -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context();
    provide_context(AuthContext::new());

    view! {
        // injects a stylesheet into the document <head>
        // id=leptos means cargo-leptos will hot-reload this stylesheet
        <Stylesheet id="leptos" href="/pkg/game-night.css"/>

        // sets the document title
        <Title text="Game Night"/>

        // content for this welcome page
        <div class="static h-screen w-screen bg-gradient-to-br from-stone-50 to-stone-200">
            <Router>
                <Navbar/>
                <main>
                    <Routes>
                        <Route path="/" view=HomePage/>
                        <Route path="/library" view=LibraryPage/>
                        <Route path="/sessions" view=SessionsPage/>
                        <Route path="/sessions/:id" view=SessionEditPage/>
                        <Route path="/register" view=RegisterPage/>
                        <Route path="/login" view=LoginPage/>
                        <Route path="/*any" view=NotFound/>
                    </Routes>
                </main>
                <div class="absolute right-0 bottom-0">
                    <BGGCredit/>
                </div>
            </Router>
        </div>
    }
}

/// Renders the home page of your application.
#[component]
fn HomePage() -> impl IntoView {
    view! {
    }
}

/// 404 - Not Found
#[component]
fn NotFound() -> impl IntoView {
    // set an HTTP status code 404
    // this is feature gated because it can only be done during
    // initial server-side rendering
    // if you navigate to the 404 page subsequently, the status
    // code will not be set because there is not a new HTTP request
    // to the server
    #[cfg(feature = "ssr")]
    {
        // this can be done inline because it's synchronous
        // if it were async, we'd use a server function
        let resp = expect_context::<leptos_actix::ResponseOptions>();
        resp.set_status(actix_web::http::StatusCode::NOT_FOUND);
    }

    view! {
        <h1>"Not Found"</h1>
    }
}
