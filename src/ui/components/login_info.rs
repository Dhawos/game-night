use crate::{api::user::{Logout, UserResponse}, providers::auth_context::AuthContext};
use crate::ui::components::atoms::buttons::{LinkButton,SensitiveSubmitButton};
use leptos::*;
use leptos_router::ActionForm;

pub fn get_user() -> impl Fn() -> Option<UserResponse> {
    let auth_context = expect_context::<AuthContext>();
    let is_loading = auth_context.user.loading();
    move || {is_loading(); auth_context.user.get().unwrap_or(Ok(None)).unwrap_or(None)}
}

#[component]
pub fn LoginInfo() -> impl IntoView {
    let auth_context = expect_context::<AuthContext>();
    view! {
        <Show
            when= move || {get_user()().is_some()}
            fallback=move || view! {<AnonymousLoginInfo/>}
        >
            <AuthenticatedLoginInfo user=get_user() logout_action=auth_context.logout/>
        </Show>
    }
}


#[component]
pub fn AnonymousLoginInfo() -> impl IntoView {
    view! {
        <div class="flex flex-row-reverse space-x-3 space-x-reverse items-center py-1 px-2">
            <LinkButton href=String::from("/login") text=String::from("Log In")/>
            <LinkButton href=String::from("/register") text=String::from("Register")/>
            <p>"Anonymous"</p>
        </div>
    }
}

#[component]
pub fn AuthenticatedLoginInfo<F: Fn() -> Option<UserResponse>>(user: F, logout_action: Action<Logout, Result<(), ServerFnError>> ) -> impl IntoView {
    view! {
        <div class="flex flex-row-reverse space-x-3 space-x-reverse items-center py-1 px-2">
            <ActionForm action=logout_action>
                <SensitiveSubmitButton text=String::from("Log out")/>
            </ActionForm>
            <p>Logged in as {user().unwrap().username}</p>
        </div>
    }
}
