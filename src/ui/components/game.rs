use crate::ui::components::login_info::get_user;
use crate::{
    api::{
        self,
        game::{list_games, CreateGame, DeleteGame, GameSuggestionResponse, SearchGames},
    },
    providers::auth_context::AuthContext,
};
use leptos::*;

#[component]
pub fn GameList() -> impl IntoView {
    let auth_context = expect_context::<AuthContext>();
    let delete_game_action = create_server_action::<DeleteGame>();
    let create_game_action = create_server_action::<CreateGame>();
    let games_list = create_resource(
        move || {
            (
                auth_context.user.get(),
                create_game_action.version().get(),
                delete_game_action.version().get(),
            )
        },
        |_| async move { list_games().await },
    );

    let list_view = move || {
        match games_list.get() {
            None => view! {<div></div>}.into_view(),
            Some(list) => 
                view! {
                {
                    list.unwrap().into_iter()
                    .map(|n| view! {
                        <li class="grid grid-cols-5 items-center py-3 px-2 space-x-2">
                            <img src={n.thumbnail} class="w-12 h-12"/>
                            <p class="font-bold">{n.name}</p>
                            <p>"👥 " {n.min_players}-{n.max_players}</p>
                            <p>"⏱ " {n.playing_time}mn</p>
                            <button class="py-1 px-2" on:click=move |_| {                                
                                delete_game_action.dispatch(DeleteGame{id: n.id});
                            }>"❌"</button>
                        </li>
                    })
                    .collect::<Vec<_>>()
                }
            }.into_view()
        }
    };

    view! {
        <Show
            when= move || {get_user()().is_some()}
            fallback=move || view! {<p> Log in to see your library </p>}
        >
            <Suspense
                fallback=move || view! { <p>"Loading..."</p> }
            >
                <div class="flex flex-col items-center">
                    <ul class="flex flex-col">
                        {list_view}
                    </ul>
                    <GameAdder action=create_game_action/>
                </div>
            </Suspense>
        </Show>
    }
}

#[component]
pub fn GameSuggestion(
    game: GameSuggestionResponse,
    action: Action<CreateGame, Result<(), ServerFnError>>,
) -> impl IntoView {
    let (game_signal, _) = create_signal(game.clone());
    let on_click = move |_: leptos::ev::MouseEvent| {
        action.dispatch(api::game::CreateGame {
            game_id: game_signal.get().id,
        });
    };

    view! {
        <li prop:value=game.id class="cursor-pointer hover:bg-red-500" on:click=on_click>{game.name}</li>
    }
}

#[component]
pub fn GameAdder(action: Action<CreateGame, Result<(), ServerFnError>>) -> impl IntoView {
    let search_game_action = create_server_action::<SearchGames>();
    let (search, set_search) = create_signal("".to_string());
    let on_change = move |ev| {
        set_search(event_target_value(&ev));
        search_game_action.dispatch(api::game::SearchGames { query: search() });
    };
    view! {
        <label>"Add a Game : "</label>
        <div class="relative m-2 w-96">
            <div class="flex flex-row-reverse items-center m-2">
                <input class="h-12 w-full" autocomplete="off" type="text" name="name" prop:value=search on:input=on_change/>
            </div>
            <Show
                when=move || {search_game_action.value().get().is_some()}
            >
                <div class="absolute w-full bg-white">
                   <ul>
                    {move || search_game_action.value().get().unwrap().unwrap().into_iter().map(|n| view!{<GameSuggestion action=action game=GameSuggestionResponse{name: n.name, id:n.id}/>}).collect_view()}
                   </ul>
                </div>
            </Show>
        </div>
    }
}
