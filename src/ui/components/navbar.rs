use leptos::*;
use crate::ui::components::login_info::LoginInfo;

#[component]
pub fn Navbar() -> impl IntoView {
    view! {
        <div class="flex bg-slate-300 drop-shadow-xl">
            <div class="flex items-center">
                <div class="flex items-center text-xl">
                    <a class="py-2 px-2 text-center hover:bg-slate-400" href="/">Home</a>
                    <a class="py-2 px-2 text-center hover:bg-slate-400" href="/sessions">Sessions</a>
                    <a class="py-2 px-2 text-center hover:bg-slate-400" href="/library">Library</a>
                </div>
            </div>
            <div class="flex items-center ml-auto">
                <LoginInfo />
            </div>
        </div>
    }
}
