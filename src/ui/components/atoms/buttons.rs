use leptos::{component, ev::MouseEvent, view, IntoView};

#[component]
pub fn LinkButton(href: String, text: String) -> impl IntoView {
    view!{
        <a class="bg-sky-500 rounded-full text-white py-1 px-2 cursor-pointer" href={href}><button>{text}</button></a>
    }
}

#[component]
pub fn SubmitButton(text: String) -> impl IntoView {
    view!{
        <input class="bg-sky-500 rounded-full text-white py-1 px-2 cursor-pointer" type="submit" value={text}/ >
    }
}

#[component]
pub fn SensitiveSubmitButton(text: String) -> impl IntoView {
    view!{
        <input class="bg-red-500 rounded-full text-white py-1 px-2 cursor-pointer" type="submit" value={text}/ >
    }
}

#[component]
pub fn ActionButton<F>(text: String, callback: F) -> impl IntoView
where
    F: FnMut(leptos::ev::MouseEvent) + 'static
{
    view!{
        <button class="bg-sky-500 rounded-full text-white py-1 px-2 cursor-pointer" on:click=callback>{text}</button>
    }
}

#[component]
pub fn SensitiveActionButton<F>(text: String, callback: F) -> impl IntoView
where
    F: FnMut(leptos::ev::MouseEvent) + 'static
{
    view!{
        <button class="bg-red-500 rounded-full text-white py-1 px-2 cursor-pointer" on:click=callback>{text}</button>
    }
}


