use leptos::{component, ev::MouseEvent, view, IntoView};

#[component]
pub fn PageHeader(text: String) -> impl IntoView {
    view!{
        <h1 class="text-2xl font-bold font-body py-3 px-2">{text}</h1>
    }
}
