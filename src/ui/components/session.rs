use crate::{api::session::DeleteSession, ui::components::atoms::buttons::LinkButton};
use leptos_router::ActionForm;
use leptos::*;
use crate::{api::session::{list_sessions, CreateSession}, providers::auth_context::AuthContext, ui::components::atoms::buttons::{ActionButton, SensitiveActionButton, SubmitButton}};
use crate::ui::components::login_info::get_user;

#[component]
pub fn SessionModal() -> impl IntoView {
    let show_modal = create_rw_signal(false);
    let create_new_session_action = create_server_action::<CreateSession>();
    let toggle_modal = move |_| {
        show_modal.set(!show_modal.get());
    };
    view! {
        <Show 
            when= move || show_modal.get()
            fallback= move || view! {<ActionButton text="Create new session".to_string() callback=toggle_modal/> }
        >
            <div class="absolute inset-0 z-50 flex items-center justify-center bg-gray-900 bg-opacity-60">
                <div class="block rounded-lg bg-white w-2/5 p-4 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] z-70">
                    <h5 class="mb-5 text-xl font-medium leading-tight text-neutral-800">
                        Plan a new session
                    </h5>
                    <ActionForm class="flex flex-col items-center gap-4" action=create_new_session_action>
                            <div class="flex">
                                <label for="datetime">When: </label> <input type="datetime-local" required name="datetime"/>
                            </div>
                            <div class="flex">
                                <label for="duration">Session Length: </label> <input type="number" required name="duration"/> <p> minutes</p>
                            </div>
                            <div class="grid grid-cols-2 gap-4">
                                <SubmitButton text="Plan session".to_string()/>
                                <SensitiveActionButton text="Cancel".to_string() callback=toggle_modal/> 
                            </div>
                    </ActionForm>
                </div>
            </div>
        </Show>
    }
}

#[component]
pub fn SessionsList() -> impl IntoView {
    let auth_context = expect_context::<AuthContext>();
    let delete_session_action = create_server_action::<DeleteSession>();
    let games_list = create_resource(
        move || {
            (
                auth_context.user.get(),
                delete_session_action.version().get(),
            )
        },
        |_| async move { list_sessions().await },
    );

    let list_view = move || {
        match games_list.get() {
            None => view! {<div></div>}.into_view(),
            Some(list) => 
                view! {
                {
                    list.unwrap().into_iter()
                    .map(|n| view! {
                        <li class="grid grid-cols-4 items-center py-3 px-2 space-x-2">
                            <p>{n.date_time.to_string()}</p>
                            <p>{n.duration}</p>
                            <p>People coming</p>
                            <div>
                                <LinkButton href=format!("/sessions/{}",n.id) text=String::from("Edit")/>
                                <button class="py-1 px-2" on:click=move |_| {                                
                                    delete_session_action.dispatch(DeleteSession{id: n.id});
                                }>"❌"</button>
                            </div>
                        </li>
                    })
                    .collect::<Vec<_>>()
                }
            }.into_view()
        }
    };

    view! {
        <Show
            when= move || {get_user()().is_some()}
            fallback=move || view! {<p> Log in to see your sessions</p>}
        >
            <ol>
                <li class="grid grid-cols-4 items-center py-3 px-2 space-x-2">
                    <p class="font-bold">date</p>
                    <p class="font-bold">Duration</p>
                    <p class="font-bold">People coming</p>
                    <p class="font-bold">Actions</p>
                </li>
                <Suspense
                    fallback=move || view! { <p>"Loading..."</p> }
                >
                    {list_view}
                </Suspense>
            </ol>
        </Show>
    }
}
