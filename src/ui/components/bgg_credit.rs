use leptos::*;

#[component]
pub fn BGGCredit() -> impl IntoView {
    view! {
        <a href="https://boardgamegeek.com"><img src="assets/bgg_credit.webp"/></a>
    }
}
