use crate::api::user::Register;
use leptos::create_server_action;
use leptos::{component, view, IntoView};
use leptos_router::ActionForm;

/// Renders the home page of your application.
#[component]
pub fn RegisterPage() -> impl IntoView {
    let create_user_action = create_server_action::<Register>();

    view! {
        <h1>"Register"</h1>
        <ActionForm action=create_user_action>
            <label for="display_name">Display Name: </label> <input type="text" required name="display_name"/>
            <label for="username">Username: </label> <input type="text" required name="username"/>
            <label for="email">E-mail: </label> <input type="email" required name="email"/>
            <label for="password">Password: </label> <input type="password" required name="password"/>
            <label for="password_confirmation">Confirm password: </label> <input type="password" required name="password_confirmation"/>
            <input type="submit" value="register"/ >
        </ActionForm>
    }
}
