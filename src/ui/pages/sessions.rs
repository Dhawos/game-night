use leptos::{expect_context, SignalWith};
use crate::api::session::get_session;
use crate::providers::auth_context::AuthContext;
use crate::ui::components::atoms::headers::PageHeader;
use crate::ui::components::session::{SessionModal, SessionsList};
use leptos::ErrorBoundary;
use leptos::SignalGet;
use leptos::Suspense;
use leptos::{component, create_resource, view, IntoView};
use leptos_router::use_params_map;

#[component]
pub fn SessionsPage() -> impl IntoView {
    view! {
        <PageHeader text="Upcoming Sessions".to_string()/>
        <SessionModal/>
        <SessionsList/>
    }
}

#[component]
pub fn SessionEditPage() -> impl IntoView {
    let auth_context = expect_context::<AuthContext>();
    let params = use_params_map();
    let id = move || params.with(|params| params.get("id").cloned().unwrap_or_default());
    let session_id = id().parse::<i32>().unwrap();

    let session_resource = create_resource(move || (auth_context.user.get()), move |_| async move { get_session(session_id).await });

    let session_view = move || {
        session_resource.get().map(|session_result| {
            session_result.map(|session_option| {
                session_option.map(|session| move || view! {<p>{session.duration}</p>})
            })
        })
    };

    view! {
        <Suspense fallback=move || view! { <p>"Loading..."</p>}.into_view()>
            <ErrorBoundary
                fallback=move |_| view! { <p>"Could not get session"</p>}.into_view()
            >
                {session_view}
            </ErrorBoundary>
        </Suspense>
    }
}
