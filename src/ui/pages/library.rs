use crate::ui::components::game::GameList;
use crate::ui::components::atoms::headers::PageHeader;
use leptos::{component, view, IntoView};

#[component]
pub fn LibraryPage() -> impl IntoView {
    view! {
        <PageHeader text="My Library".to_string()/>
        <GameList/>
    }
}
