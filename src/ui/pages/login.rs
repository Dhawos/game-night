use leptos::ErrorBoundary;
use crate::providers::auth_context::AuthContext;
use leptos::{expect_context, SignalGet};
use leptos::{component, view, IntoView};
use leptos_router::ActionForm;

/// Renders the home page of your application.
#[component]
pub fn LoginPage() -> impl IntoView {
    let auth_context = expect_context::<AuthContext>();
    let login_response = auth_context.login.value();
    view! {
        <h1>"Login"</h1>
        <ActionForm action=auth_context.login>
            <label for="username">Username: </label> <input type="text" required name="username"/>
            <label for="password">Password: </label> <input type="password" required name="password"/>
            <input class="cursor-pointer" type="submit" value="Log in"/ >
        </ActionForm>
        <ErrorBoundary 
            fallback=|errors| view! {
                <div class="text-red-500">
                    {move || errors.get()
                        .into_iter()
                        .map(|(_, e)| view! { <li>{e.to_string()}</li>})
                        .collect::<Vec<_>>()
                    }
                </div>
        }>
            {login_response}
        </ErrorBoundary>
    }
}
