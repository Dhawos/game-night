use super::schema::bgg_games;
use super::repository::DbPool;
use crate::db::schema::bgg_games::dsl::*;
use crate::db::utils::lower;
use diesel::prelude::*;
use leptos::ServerFnError;
use serde::{Deserialize, Serialize};


#[derive(Queryable, Insertable, Identifiable, Selectable, Serialize, Deserialize, Clone, Debug, AsChangeset)]
#[diesel(table_name = crate::db::schema::bgg_games)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct BGGGame {
    pub id: i32,
    pub name: String,
    pub year_published: i32,
    pub description: Option<String>,
    pub thumbnail: Option<String>,
    pub image: Option<String>,
    pub min_players: Option<i32>,
    pub max_players: Option<i32>,
    pub playing_time: Option<i32>,
}

impl BGGGame {
    pub async fn search_games(query: String, pool: &DbPool) -> Result<Vec<BGGGame>, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let pattern = format!("%{}%", query);
        let results = bgg_games
            .filter(lower(name).like(pattern))
            .limit(10)
            .select(BGGGame::as_select())
            .load(connection)?;
        Ok(results)
    }

    pub async fn create_game(&self, pool: &DbPool) -> Result<BGGGame, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let result = diesel::insert_into(bgg_games::table)
            .values(self)
            .returning(BGGGame::as_returning())
            .get_result(connection)?;
        Ok(result)
    }
    
    pub async fn update_game(&self, pool: &DbPool) -> Result<(), ServerFnError> {
        let connection = &mut pool.get().unwrap();
        diesel::update(self).set(self).execute(connection)?;
        Ok(())
    }
    
    pub async fn get_by_id(game_id: i32, pool: &DbPool) -> Result<Option<BGGGame>, ServerFnError> {
        use bgg_games::*;
        let connection = &mut pool.get().unwrap();
        table
            .find(game_id)
            .first(connection)
            .optional()
            .map_err(ServerFnError::new)
    }
}

