use crate::db::user::User;
use super::{
    repository::DbPool,
    schema::{self, bgg_games},
};
use crate::db::schema::games::dsl::*;
use crate::db::bgg_game::BGGGame;
use diesel::prelude::*;
use leptos::ServerFnError;
use serde::{Deserialize, Serialize};

#[derive(
    Associations, Queryable, Identifiable, Selectable, Serialize, Deserialize, Clone, Debug
)]
#[diesel(table_name = crate::db::schema::games)]
#[diesel(belongs_to(User, foreign_key=owner_id))]
#[diesel(belongs_to(BGGGame, foreign_key=bgg_game_id))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Game {
    pub id: i32,
    pub owner_id: i32,
    pub bgg_game_id: i32,
}
#[derive(Insertable, Serialize, Deserialize, Debug)] #[diesel(table_name = schema::games)]
pub struct NewGame {
    pub owner_id: i32,
    pub bgg_game_id: i32,
}

impl Game {
    pub async fn query_games(
        pool: &DbPool,
        user: &User,
    ) -> Result<Vec<(Game, BGGGame)>, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let results = Game::belonging_to(&user)
            .limit(10)
            .inner_join(bgg_games::table)
            .select((Game::as_select(), BGGGame::as_select()))
            .load::<(Game, BGGGame)>(connection)?;
        Ok(results)
    }

    pub async fn delete_by_id(game_id: i32, pool: &DbPool) -> Result<(), ServerFnError> {
        let connection = &mut pool.get().unwrap();
        diesel::delete(games.filter(id.eq(game_id))).execute(connection)?;
        Ok(())
    }
}

impl NewGame {
    pub async fn create_game(&self, pool: &DbPool) -> Result<Game, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let result = diesel::insert_into(schema::games::table)
            .values(self)
            .returning(Game::as_returning())
            .get_result(connection)?;
        Ok(result)
    }
}
