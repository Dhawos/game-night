// @generated automatically by Diesel CLI.

diesel::table! {
    bgg_games (id) {
        id -> Int4,
        name -> Varchar,
        year_published -> Int4,
        description -> Nullable<Varchar>,
        thumbnail -> Nullable<Varchar>,
        image -> Nullable<Varchar>,
        min_players -> Nullable<Int4>,
        max_players -> Nullable<Int4>,
        playing_time -> Nullable<Int4>,
    }
}

diesel::table! {
    game_suggestions (user_id, session_id) {
        user_id -> Int4,
        session_id -> Int4,
        game_id -> Int4,
        accepted -> Bool,
    }
}

diesel::table! {
    games (id) {
        id -> Int4,
        owner_id -> Int4,
        bgg_game_id -> Int4,
    }
}

diesel::table! {
    session_invitations (user_id, session_id) {
        user_id -> Int4,
        session_id -> Int4,
        accepted -> Bool,
    }
}

diesel::table! {
    sessions (id) {
        id -> Int4,
        owner_id -> Int4,
        date_time -> Timestamptz,
        duration -> Int4,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        display_name -> Varchar,
        hashed_password -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::joinable!(game_suggestions -> games (game_id));
diesel::joinable!(game_suggestions -> sessions (session_id));
diesel::joinable!(game_suggestions -> users (user_id));
diesel::joinable!(games -> bgg_games (bgg_game_id));
diesel::joinable!(games -> users (owner_id));
diesel::joinable!(session_invitations -> sessions (session_id));
diesel::joinable!(session_invitations -> users (user_id));
diesel::joinable!(sessions -> users (owner_id));

diesel::allow_tables_to_appear_in_same_query!(
    bgg_games,
    game_suggestions,
    games,
    session_invitations,
    sessions,
    users,
);
