use crate::db::user::User;
use leptos::ServerFnError;
use crate::db::repository::DbPool;
use diesel::prelude::*;
use crate::db::schema::sessions::dsl::*;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};


#[derive(
    Associations, Queryable, Identifiable, Selectable, Serialize, Deserialize, Clone, Debug
)]
#[diesel(table_name = crate::db::schema::sessions)]
#[diesel(belongs_to(User, foreign_key=owner_id))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Session {
    pub id: i32,
    pub owner_id: i32,
    pub date_time: NaiveDateTime,
    pub duration: i32,
}

#[derive(Insertable, Serialize, Deserialize, Debug)]
#[diesel(table_name = crate::db::schema::sessions)]
pub struct NewSession {
    pub owner_id: i32,
    pub date_time: NaiveDateTime,
    pub duration: i32,
}

impl Session {
    pub async fn query_sessions(
        pool: &DbPool,
        user: &User,
    ) -> Result<Vec<Session>, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let results = Session::belonging_to(&user)
            .limit(10)
            .select(Session::as_select())
            .load::<Session>(connection)?;
        Ok(results)
    }

    pub fn delete_by_id(session_id: i32, pool: &DbPool) -> Result<(), ServerFnError> {
        let connection = &mut pool.get().unwrap();
        diesel::delete(sessions.filter(id.eq(session_id))).execute(connection)?;
        Ok(())
    }

    pub fn get_by_id(session_id: i32, pool: &DbPool) -> Result<Option<Session>, ServerFnError> {
        use crate::db::schema::sessions::table;
        let connection = &mut pool.get().unwrap();
        table
            .find(session_id)
            .first(connection)
            .optional()
            .map_err(ServerFnError::new)
    }
}

impl NewSession {
    pub fn validate(&self) -> bool {
        true
    }

    pub async fn create_session(&self, pool: &DbPool) -> Result<Session, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let result = diesel::insert_into(crate::db::schema::sessions::table)
            .values(self)
            .returning(Session::as_returning())
            .get_result(connection)?;
        Ok(result)
    }
}

#[derive(
    Associations, Queryable, Identifiable, Selectable, Serialize, Deserialize, Clone, Debug
)]
#[diesel(table_name = crate::db::schema::session_invitations)]
#[diesel(belongs_to(User, foreign_key=user_id))]
#[primary_key(user_id,session_id)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct SessionInvitaion {
    pub user_id: i32,
    pub session_id: i32,
    pub accepted: bool,
}

#[derive(Insertable, Serialize, Deserialize, Debug)]
#[diesel(table_name = crate::db::schema::session_invitations)]
pub struct NewSessionInvitation {
    pub user_id: i32,
    pub session_id: i32,
    accepted: bool,
}

impl NewSessionInvitation {
    pub fn new(invited_user_id: i32, session_id: i32) -> Self {
        Self {
            user_id: invited_user_id,
            session_id,
            accepted: false, 
        }
    }

    pub fn create_invitation(&self, pool: &DbPool) -> Result<SessionInvitaion, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let result = diesel::insert_into(crate::db::schema::session_invitations::table)
            .values(self)
            .returning(SessionInvitaion::as_returning())
            .get_result(connection)?;
        Ok(result)
    }
}
