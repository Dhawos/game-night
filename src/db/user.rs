use chrono::DateTime;
use super::{
    repository::DbPool,
    schema::users,
};
use chrono::Utc;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use leptos::ServerFnError;

#[derive(Queryable, Selectable, Identifiable, Serialize, Deserialize, Debug, Clone)]
#[diesel(table_name = users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub display_name: String,
    pub hashed_password: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Insertable, Serialize, Deserialize, Debug)]
#[diesel(table_name = users)]
pub struct NewUser {
    pub username: String,
    pub email: String,
    pub display_name: String,
    pub hashed_password: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

impl NewUser {
    pub fn new(username: &str, email: &str, display_name: &str, hashed_password: &str) -> NewUser {
        NewUser {
            username: username.to_owned(),
            email: email.to_owned(),
            display_name: display_name.to_owned(),
            hashed_password: hashed_password.to_owned(),
            created_at: Utc::now(),
            updated_at: Utc::now(),
        }
    }
    pub async fn create(&self, pool: &DbPool) -> Result<User, ServerFnError> {
        let connection = &mut pool.get().unwrap();
        let result = diesel::insert_into(users::table)
            .values(self)
            .returning(User::as_returning())
            .get_result(connection)?;
        Ok(result)
    }
}

impl User {
    pub fn get_by_username(user_name: &str, pool: &DbPool) -> Result<Option<User>, ServerFnError> {
        use users::*;
        let connection = &mut pool.get().unwrap();
        table
            .filter(username.eq(user_name))
            .select(User::as_select())
            .first(connection)
            .optional()
            .map_err(ServerFnError::new)
    }

    pub fn get_by_id(user_id: i32, pool: &DbPool) -> Result<Option<User>, ServerFnError> {
        use users::*;
        let connection = &mut pool.get().unwrap();
        table
            .find(user_id)
            .first(connection)
            .optional()
            .map_err(ServerFnError::new)
    }
}
