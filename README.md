# Game Night 

## What is this project ?

This project aims to facilitate organizing boardgaming sessions by allowing users to decide which games they want to bring depending on the session length,
people preferences and games' complexity.

It is mostly an excuse to learn Rust and it's ecosystem in an actual project and have fun.

## The stack

* Leptos for UI
* Tailwind for styling
* Actix as the Webserver
* Diesel as an ORM

## Setting up the project

### Installing dependencies

1. `rustup toolchain install nightly --allow-downgrade` - make sure you have Rust nightly
2. `rustup target add wasm32-unknown-unknown` - add the ability to compile Rust to WebAssembly

### Running background services

The program needs a postgres database to run, you can start it locally along with the `pgAdmin` tool to edit/view the data with the provided `docker-compose`. 
To do so simply run 

```
docker-compose up -d
```

### Preparing the database

The database needs game data to be loaded, for this you need to download BoardGameGeek's CSV export available here : https://boardgamegeek.com/data_dumps/bg_ranks
Once it is done simply run the binary crate to load the CSV into the database.

```
cargo run --bin parse-bgg-export --features ssr bgg_dumps/boardgames_ranks.csv
```

## Running the server locally

Once everything is settup and the `postgres` container is running, you can start the server by running : 

```
cargo leptos watch
```

This will start the server with auto-reload enabled at `https://localhost:3000`.

## Credits

This project makes use of the BoardGameGeek API.

[![BoardGameGeek](/assets/bgg_credit.webp 'BoardGameGeek')](https://boardgamegeek.com)
