CREATE TABLE users (
  id SERIAL PRIMARY KEY NOT NULL,
  username VARCHAR NOT NULL UNIQUE,
  email VARCHAR NOT NULL UNIQUE,
  display_name VARCHAR NOT NULL,
  hashed_password VARCHAR NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE bgg_games (
  id INTEGER PRIMARY KEY,
  name VARCHAR NOT NULL,
  year_published INTEGER NOT NULL,
  description VARCHAR,
  thumbnail VARCHAR,
  image VARCHAR,
  min_players INTEGER,
  max_players INTEGER,
  playing_time INTEGER
);

CREATE TABLE games (
  id SERIAL PRIMARY KEY,
  owner_id SERIAL NOT NULL REFERENCES users(id),
  bgg_game_id SERIAL NOT NULL REFERENCES bgg_games(id)
);

CREATE TABLE sessions (
  id SERIAL PRIMARY KEY,
  owner_id SERIAL NOT NULL REFERENCES users(id),
  date_time TIMESTAMP WITH TIME ZONE NOT NULL,
  duration INTEGER NOT NULL
);

CREATE TABLE session_invitations (
  PRIMARY KEY (user_id, session_id),
  user_id SERIAL NOT NULL REFERENCES users(id),
  session_id SERIAL NOT NULL REFERENCES sessions(id),
  accepted BOOLEAN NOT NULL
);

CREATE TABLE game_suggestions (
  PRIMARY KEY (user_id, session_id),
  user_id SERIAL NOT NULL REFERENCES users(id),
  session_id SERIAL NOT NULL REFERENCES sessions(id),
  game_id SERIAL NOT NULL REFERENCES games(id),
  accepted BOOLEAN NOT NULL
);

